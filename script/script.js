btnstart = true;
btnstop = false;
btnclear = false;

var startDate;
var endDate;

function Start() {
  document.getElementById("st").innerHTML = "Start";
}

function btnClick() {
  if (btnstart) {
    startDate = new Date();

    document.getElementById("btn").innerHTML = "Stop";
    document.getElementById("stime").innerHTML = startDate.toLocaleTimeString(
      [],
      { hour: "2-digit", minute: "2-digit" }
    );
    btnstart = false;
    btnstop = true;
    btnclear = false;
  } else if (btnstop) {
    endDate = new Date();
    document.getElementById("btn").innerHTML = "Clear";
    document.getElementById("stoptime").innerHTML = endDate.toLocaleTimeString(
      [],
      { hour: "2-digit", minute: "2-digit" }
    );
    btnstop = false;
    btnclear = true;
    btnstart = false;
    var minutes = Math.round(
      (endDate.getTime() - startDate.getTime()) / 1000 / 60
    );

    let counter = Math.floor(minutes / 60);
    let remaining = Math.abs(minutes % 60);

    if (remaining < 16) {
      document.getElementById("getTotal").innerHTML = (counter * 1500) + 500;
    } else if (remaining < 31) {
      document.getElementById("getTotal").innerHTML = (counter * 1500) + 1000;
    } else {
      document.getElementById("getTotal").innerHTML = (counter * 1500) + 1500;
    }

    document.getElementById("getMinute").innerHTML = minutes;
  } else if(btnclear) {
    document.getElementById("btn").innerHTML = " Start";
    btnstart = true;
    btnclear = false;
    btnstop = false;
    document.getElementById("getMinute").innerHTML = "0";
    document.getElementById("getTotal").innerHTML = "0";
  }
}

let myTimer = setInterval(function () {
  currenttime();
}, 1000);
function currenttime() {
  var d = new Date();
  var d1 = d.toDateString() + " " + d.toLocaleTimeString();
  document.getElementById("current").innerHTML = d1;
}
